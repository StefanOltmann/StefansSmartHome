# Stefans Smart Home

**<font color="red">PROTOYPE</font>**

## Motivation

I'm a software developer who owns a KNX-based smart home.

Today I'm using **[openHAB](https://www.openhab.org/)** with a **Gira X1**, but this is not as reliable and lightweight
as I want it to be.

Looking for a nice freetime project where I can code some Kotlin and play around with the Alexa SDK and Vaadin I decided
to start my own Smart Home project at a much smaller scale.

## Status

This is in **early alpha** at a **prototype stage**.

But the code is also used in production at my house.

## Languages

All code is written in Kotlin! <3

## Modules

| Module       | Description              | Frameworks              |
| -------------|--------------------------|-------------------------|
| alexaskill   | Alexa Smart Home Skill   | [AWS Lambda Framework](https://docs.aws.amazon.com/lambda/latest/dg/lambda-java.html) |
| server       | KNX server with REST API | [Ktor](https://ktor.io/) & [knx-link](https://github.com/pitschr/knx-link) |
| webapp       | Progressive WebApp       | [Vaadin](https://vaadin.com/) |

The Android App is located in a [separate repository](https://github.com/StefanOltmann/smart-home-app-android).

## Documentation

As soon as this project is in a stable state I'll provide some documentation so that anybody who wants can also use this
project in KNX-based smart homes.

## License

Since depending on knx-link which is GPLv3 licensed this code is also under that license.