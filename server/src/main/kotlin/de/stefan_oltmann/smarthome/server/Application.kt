/*
 * Stefans Smart Home Project
 * Copyright (C) 2021 Stefan Oltmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.stefan_oltmann.smarthome.server

import de.stefan_oltmann.smarthome.server.data.DeviceRepository
import de.stefan_oltmann.smarthome.server.data.DeviceStateRepository
import de.stefan_oltmann.smarthome.server.knx.KnxService
import de.stefan_oltmann.smarthome.server.model.DeviceId
import de.stefan_oltmann.smarthome.server.model.DevicePowerState
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.websocket.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

/* TODO FIXME Change logging. This way is not good. */
val logger: Logger = LoggerFactory.getLogger(Application::class.java)

const val HEADER_KEY_AUTH_CODE = "AUTH_CODE"

fun main(args: Array<String>) {

    try {

        io.ktor.server.netty.EngineMain.main(args)

    } catch (ex: Throwable) {
        logger.error("Application crashed.", ex)
    }
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.main(testing: Boolean = false) {

    logger.info("Starting...")

    /* Headless mode per default as we will not show a GUI. */
    System.setProperty("java.awt.headless", "true")

    val authCodeFile = File("auth_code.txt")

    if (!authCodeFile.exists()) {
        logger.error("File 'auth_code.txt' is missing. Can't boot up.")
        return
    }

    val authCode = authCodeFile.readText()

    logger.info("Starting KNX Service...")

    val deviceRepository = DeviceRepository()

    val deviceStateRepository = DeviceStateRepository()

    val knxService = KnxService(deviceRepository, deviceStateRepository)

    /* Read all state initially */
    knxService.readAllDeviceStates()

    /* Install WebSockets */
//    install(WebSockets) {
//        pingPeriod = Duration.ofSeconds(15)
//        timeout = Duration.ofSeconds(15)
//        maxFrameSize = Long.MAX_VALUE
//        masking = false
//    }

    install(ContentNegotiation) {
        jackson {
        }
    }

    routing {

        get("/devices") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                logger.info("[API] | Requesting device list...")

                call.respond(deviceRepository.devices)

            } catch (ex: Exception) {
                logger.error("Requesting device list failed.", ex)
                call.respond(HttpStatusCode.InternalServerError)
            }
        }

        get("/devices/current-states") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                logger.info("[API] | Requesting current device states...")

                call.respond(deviceStateRepository.states)

            } catch (ex: Exception) {
                logger.error("Requesting current device states failed.", ex)
                call.respond(HttpStatusCode.InternalServerError)
            }
        }

        get("/devices/state-history") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                logger.info("[API] | Requesting device state history...")

                call.respond(deviceStateRepository.history)

            } catch (ex: Exception) {
                logger.error("Requesting device state history failed.", ex)
                call.respond(HttpStatusCode.InternalServerError)
            }
        }

        get("/device/{deviceId}/set/power-state/value/{powerState}") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                val deviceId = DeviceId(call.parameters["deviceId"]!!)
                val powerState: DevicePowerState = DevicePowerState.valueOf(call.parameters["powerState"]!!)

                logger.info("[API] | SET ${deviceId.value} TO $powerState")

                val device = deviceRepository.findById(deviceId)

                if (device == null) {
                    call.respond(HttpStatusCode.BadRequest, "Device does not exist.")
                    return@get
                }

                /* Write to KNX bus first. */
                knxService.writePowerState(device, powerState)

                /* Update local object after the successful write to KNX bus. */
                deviceStateRepository.updatePowerState(device.id, powerState)

                call.respond(HttpStatusCode.OK, "Device updated.")

            } catch (ex: Exception) {
                logger.error("Setting power state failed.", ex)
                call.respond(HttpStatusCode.InternalServerError, "Could not set new state.")
            }
        }

        get("/device/{deviceId}/set/percentage/value/{percentage}") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                val deviceId = DeviceId(call.parameters["deviceId"]!!)
                val percentage = call.parameters["percentage"]!!.toInt()

                logger.info("[API] | SET ${deviceId.value} TO $percentage%")

                val device = deviceRepository.findById(deviceId)

                if (device == null) {
                    call.respond(HttpStatusCode.BadRequest, "Device does not exist.")
                    return@get
                }

                /* Write to KNX bus first. */
                knxService.writePercentage(device, percentage)

                /* Update local object after the successful write to KNX bus. */
                deviceStateRepository.updatePercentage(device.id, percentage)

                call.respond(HttpStatusCode.OK, "Device updated.")

            } catch (ex: Exception) {
                logger.error("Setting percentage failed.", ex)
                call.respond(HttpStatusCode.InternalServerError, "Could not set new state.")
            }
        }

        get("/device/{deviceId}/set/target-temperature/value/{temperature}") {

            try {

                if (!checkAuth(call, authCode))
                    return@get

                val deviceId = DeviceId(call.parameters["deviceId"]!!)
                val temperature = call.parameters["temperature"]!!.toInt()

                logger.info("[API] | SET ${deviceId.value} TO $temperature °C")

                val device = deviceRepository.findById(deviceId)

                if (device == null) {
                    call.respond(HttpStatusCode.BadRequest, "Device does not exist.")
                    return@get
                }

                /* Write to KNX bus first. */
                knxService.writeTargetTemperature(device, temperature.toDouble())

                /* Update local object after the successful write to KNX bus. */
                deviceStateRepository.updateTargetTemperature(device.id, temperature.toDouble())

                call.respond(HttpStatusCode.OK, "Device updated.")

            } catch (ex: Exception) {
                logger.error("Setting target temperature failed.", ex)
                call.respond(HttpStatusCode.InternalServerError, "Could not set new state.")
            }
        }

//        webSocket("/websocket") {
//
//            send(Frame.Text("Hi from server"))
//
//            while (true) {
//
//                val frame = incoming.receive()
//
//                if (frame is Frame.Text)
//                    send(Frame.Text("Client said: " + frame.readText()))
//            }
//        }
    }
}

private suspend fun checkAuth(call: ApplicationCall, authCode: String): Boolean {

    val requestAuthCode = call.request.header(HEADER_KEY_AUTH_CODE)

    if (authCode != requestAuthCode) {

        logger.warn("Unauthorized call from '${call.request.origin.remoteHost}' for '${call.request.origin.uri}'.")

        call.respond(HttpStatusCode.Unauthorized, "Unauthorized")

        return false
    }

    return true
}