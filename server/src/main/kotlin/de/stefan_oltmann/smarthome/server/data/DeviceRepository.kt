/*
 * Stefans Smart Home Project
 * Copyright (C) 2021 Stefan Oltmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.stefan_oltmann.smarthome.server.data

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.stefan_oltmann.smarthome.server.model.Device
import de.stefan_oltmann.smarthome.server.model.DeviceId
import de.stefan_oltmann.smarthome.server.model.GroupAddressType
import java.io.File
import java.util.*

class DeviceRepository {

    val devices: List<Device>

    private val gson by lazy {

        val builder = GsonBuilder()
        builder.setPrettyPrinting()
        builder.create()
    }

    init {

        val devicesFile = File("devices.json")

        val listType = object : TypeToken<ArrayList<Device>>() {}.type
        devices = gson.fromJson(devicesFile.readText(), listType)

        println("Started with ${devices.size} devices.")
    }

    fun findById(deviceId: DeviceId): Device? {

        for (device in devices)
            if (device.id == deviceId)
                return device

        return null
    }

    fun findDeviceAndType(groupAddress: String): Pair<Device, GroupAddressType>? {

        for (device in devices) {

            when (groupAddress) {
                device.gaPowerStateWrite -> return Pair(device, GroupAddressType.POWER_STATE_WRITE)
                device.gaPowerStateStatus -> return Pair(device, GroupAddressType.POWER_STATE_STATUS)
                device.gaPercentageWrite -> return Pair(device, GroupAddressType.PERCENTAGE_WRITE)
                device.gaPercentageStatus -> return Pair(device, GroupAddressType.PERCENTAGE_STATUS)
                device.gaCurrentTemperature -> return Pair(device, GroupAddressType.CURRENT_TEMPERATURE)
                device.gaTargetTemperature -> return Pair(device, GroupAddressType.TARGET_TEMPERATURE)
            }
        }

        return null
    }
}