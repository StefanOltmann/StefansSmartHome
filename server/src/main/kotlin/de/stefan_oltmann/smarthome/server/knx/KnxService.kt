/*
 * Stefans Smart Home Project
 * Copyright (C) 2021 Stefan Oltmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.stefan_oltmann.smarthome.server.knx

import de.stefan_oltmann.smarthome.server.data.DeviceRepository
import de.stefan_oltmann.smarthome.server.data.DeviceStateRepository
import de.stefan_oltmann.smarthome.server.logger
import de.stefan_oltmann.smarthome.server.model.Device
import de.stefan_oltmann.smarthome.server.model.DevicePowerState
import de.stefan_oltmann.smarthome.server.model.GroupAddressType
import li.pitschmann.knx.core.address.GroupAddress
import li.pitschmann.knx.core.body.Body
import li.pitschmann.knx.core.body.TunnelingRequestBody
import li.pitschmann.knx.core.communication.DefaultKnxClient
import li.pitschmann.knx.core.communication.KnxClient
import li.pitschmann.knx.core.config.ConfigBuilder
import li.pitschmann.knx.core.datapoint.DPT1
import li.pitschmann.knx.core.datapoint.DPT5
import li.pitschmann.knx.core.datapoint.DPT9
import li.pitschmann.knx.core.exceptions.DataPointTypeIncompatibleBytesException
import li.pitschmann.knx.core.plugin.ObserverPlugin
import li.pitschmann.knx.core.plugin.audit.FileAuditPlugin
import li.pitschmann.knx.core.utils.Sleeper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.Exception
import java.lang.IllegalStateException

val powerStateDpt: DPT1 = DPT1.BOOL
val percentageDpt: DPT5 = DPT5.SCALING
val temperatureDpt: DPT9 = DPT9.TEMPERATURE

class KnxService(
    private val deviceRepository: DeviceRepository,
    deviceStateRepository: DeviceStateRepository
) {

    private var knxClient: KnxClient

    init {

        val groupMonitorPlugin = GroupMonitorPlugin(deviceRepository, deviceStateRepository)

        val auditPlugin = FileAuditPlugin()

        val config = ConfigBuilder
            .tunneling()
            .plugin(groupMonitorPlugin)
            .plugin(auditPlugin)
            .build()

        knxClient = DefaultKnxClient.createStarted(config)

        /*
         * Keep the KNX client running forever on a separate thread.
         *
         * TODO FIXME Make this a coroutine.
         */
        Thread {

            while (true) {

                try {

                    knxClient.use { client ->

                        while (client.isRunning)
                            Sleeper.seconds(1)
                    }

                } catch (ex: Exception) {
                    logger.error("Service stopped due to an exception. Will restart now.", ex)
                }

                /*
                 * If this code is reached the KNX client threw an
                 * exception, was likely not running anymore and
                 * needs a fresh start.
                 */

                Sleeper.seconds(3)

                logger.info("Restarting KNX client...")

                if (!knxClient.isRunning)
                    knxClient = DefaultKnxClient.createStarted(config)
            }

        }.start()
    }

    fun readAllDeviceStates() {

        /*
         * Sending a read request to all known devices will
         * trigger responses on the KNX bus which will get
         * caught by the group monitor.
         */

        for (device in deviceRepository.devices) {

            device.gaPowerStateStatus?.let { groupAddress ->
                knxClient.readRequest(GroupAddress.of(groupAddress))
            }

            device.gaPercentageStatus?.let { groupAddress ->
                knxClient.readRequest(GroupAddress.of(groupAddress))
            }

            device.gaCurrentTemperature?.let { groupAddress ->
                knxClient.readRequest(GroupAddress.of(groupAddress))
            }

            device.gaTargetTemperature?.let { groupAddress ->
                knxClient.readRequest(GroupAddress.of(groupAddress))
            }
        }
    }

    fun writePowerState(device: Device, powerState: DevicePowerState) {

        knxClient.writeRequest(
            GroupAddress.of(device.gaPowerStateWrite),
            powerStateDpt.of(powerState == DevicePowerState.ON)
        )
    }

    fun writePercentage(device: Device, percentage: Int) {

        knxClient.writeRequest(
            GroupAddress.of(device.gaPercentageWrite),
            percentageDpt.of(percentage)
        )
    }

    fun writeTargetTemperature(device: Device, temperature: Double) {

        knxClient.writeRequest(
            GroupAddress.of(device.gaTargetTemperature),
            temperatureDpt.of(temperature)
        )
    }

    class GroupMonitorPlugin(
        private val deviceRepository: DeviceRepository,
        private val deviceStateRepository: DeviceStateRepository
    ) : ObserverPlugin {

        private val logger: Logger = LoggerFactory.getLogger(GroupMonitorPlugin::class.java)

        override fun onInitialization(client: KnxClient) {
            logger.info("Initialized by client: $client")
        }

        override fun onIncomingBody(item: Body) {
            handleItem(item)
        }

        override fun onOutgoingBody(item: Body) {
            /* We only want to handle incoming bodies. */
        }

        private fun handleItem(item: Body) {

            /*
             * Only look for this status updates and ignore other service types
             * (e.g. search, description, connect, disconnect)
             */
            if (item !is TunnelingRequestBody)
                return

            /*
             * Ignore read requests. We want status updates.
             */
            if (item.cemi.data.isEmpty())
                return

            val groupAddress = item.cemi.destinationAddress as GroupAddress

            /*
             * Find the corresponding device in the registry/repository or ignore this event.
             */
            val deviceAndType = deviceRepository.findDeviceAndType(groupAddress.addressLevel3)
                ?: return

            val device = deviceAndType.first
            val type = deviceAndType.second

            if (type == GroupAddressType.POWER_STATE_STATUS) {

                try {

                    val value = powerStateDpt.of(item.cemi.data).value

                    val powerState = if (value) DevicePowerState.ON else DevicePowerState.OFF

                    deviceStateRepository.updatePowerState(device.id, powerState)

                } catch (ex: DataPointTypeIncompatibleBytesException) {
                    logger.error("GA ${groupAddress.addressLevel3} is not of type DPT1: ${item.cemi.data.toHex()}", ex)
                }
            }

            if (type == GroupAddressType.PERCENTAGE_STATUS) {

                try {

                    val percentage = percentageDpt.of(item.cemi.data).value

                    deviceStateRepository.updatePercentage(device.id, percentage)

                } catch (ex: DataPointTypeIncompatibleBytesException) {
                    logger.error("GA ${groupAddress.addressLevel3} is not of type DPT5: ${item.cemi.data.toHex()}", ex)
                }
            }

            if (type == GroupAddressType.CURRENT_TEMPERATURE) {

                try {

                    val temperature = temperatureDpt.of(item.cemi.data).value

                    deviceStateRepository.updateCurrentTemperature(device.id, temperature)

                } catch (ex: DataPointTypeIncompatibleBytesException) {
                    logger.error("GA ${groupAddress.addressLevel3} is not of type DPT9: ${item.cemi.data.toHex()}", ex)
                }
            }

            if (type == GroupAddressType.TARGET_TEMPERATURE) {

                try {

                    val temperature = temperatureDpt.of(item.cemi.data).value

                    deviceStateRepository.updateTargetTemperature(device.id, temperature)

                } catch (ex: DataPointTypeIncompatibleBytesException) {
                    logger.error("GA ${groupAddress.addressLevel3} is not of type DPT9: ${item.cemi.data.toHex()}", ex)
                }
            }
        }

        override fun onError(throwable: Throwable) {
            logger.error("KNX client error.", throwable)
        }
    }
}