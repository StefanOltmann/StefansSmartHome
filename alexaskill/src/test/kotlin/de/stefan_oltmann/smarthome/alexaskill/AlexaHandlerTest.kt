/*
 * Stefans Smart Home Project
 * Copyright (C) 2021 Stefan Oltmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.stefan_oltmann.smarthome.alexaskill

import com.google.gson.Gson
import de.stefan_oltmann.smarthome.alexaskill.alexamodel.AlexaRequest
import de.stefan_oltmann.smarthome.alexaskill.model.Device
import de.stefan_oltmann.smarthome.alexaskill.model.DevicePowerState
import de.stefan_oltmann.smarthome.alexaskill.model.DeviceType
import de.stefan_oltmann.smarthome.alexaskill.network.RestApi
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import retrofit2.Call
import retrofit2.Response

internal class AlexaHandlerTest {

    /**
     * The handler in unit testing mode returns static messageIds and timestamps.
     *
     * This way we can easily just compare JSON input and output.
     */
    private val handler = AlexaHandler().apply { unitTesting = true }

    /**
     * A namespace that is not handled by this skill should result in an proper error response.
     */
    @Test
    fun `Handle invalid Request`() {

        val requestJson = readJson("invalid_directive.json")

        val restApiMock = mock(RestApi::class.java)

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("invalid_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        verifyNoInteractions(restApiMock)
    }

    /**
     * We always accept authorization requests.
     *
     * See https://developer.amazon.com/en-US/docs/alexa/device-apis/alexa-authorization.html
     */
    @Test
    fun `Handle Authorization Request`() {

        val requestJson = readJson("authorization_directive.json")

        val restApiMock = mock(RestApi::class.java)

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("authorization_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        verifyNoInteractions(restApiMock)
    }

    /**
     * This checks the device discovery routine.
     */
    @Test
    fun `Handle Discovery Request`() {

        val requestJson = readJson("discovery_directive.json")

        /*
         * Return mock data if the restApi is called.
         */

        val devices = listOf(
            Device(
                "power_plug",
                "Switchable device",
                DeviceType.LIGHT_SWITCH
            ),
            Device(
                "dimmer",
                "Dimmer",
                DeviceType.DIMMER
            ),
            Device(
                "roller_shutter",
                "Roller shutter",
                DeviceType.ROLLER_SHUTTER
            )
        )

        val restApiMock = mock(RestApi::class.java)

        val callMock = mock(Call::class.java) as Call<List<Device>>

        `when`(restApiMock.findAllDevices()).thenReturn(callMock)
        `when`(callMock.execute()).thenReturn(Response.success(devices))

        /*
         * Check response
         */

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("discovery_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        /*
         * Verify mocks
         */

        verify(restApiMock, times(1)).findAllDevices()
        verifyNoMoreInteractions(restApiMock)
    }

    /**
     * This turns a light on.
     */
    @Test
    fun `Handle Power Controller Request`() {

        val requestJson = readJson("power_controller_directive.json")

        /*
         * Return mock data if the restApi is called.
         */

        val restApiMock = mock(RestApi::class.java)

        val callMock = mock(Call::class.java) as Call<Unit>

        `when`(restApiMock.setDevicePowerState("my_light_switch", DevicePowerState.ON)).thenReturn(callMock)
        `when`(callMock.execute()).thenReturn(Response.success(null))

        /*
         * Check response
         */

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("power_controller_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        /*
         * Verify mocks
         */

        verify(restApiMock, times(1)).setDevicePowerState("my_light_switch", DevicePowerState.ON)
        verifyNoMoreInteractions(restApiMock)
    }

    /**
     * Dim a light.
     */
    @Test
    fun `Handle Percentage Controller Request`() {

        val requestJson = readJson("percentage_controller_directive.json")

        /*
         * Return mock data if the restApi is called.
         */

        val restApiMock = mock(RestApi::class.java)

        val callMock = mock(Call::class.java) as Call<Unit>

        `when`(restApiMock.setDevicePercentage("my_dimmer", 66)).thenReturn(callMock)
        `when`(callMock.execute()).thenReturn(Response.success(null))

        /*
         * Check response
         */

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("percentage_controller_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        /*
         * Verify mocks
         */

        verify(restApiMock, times(1)).setDevicePercentage("my_dimmer", 66)
        verifyNoMoreInteractions(restApiMock)
    }

    /**
     * Set a target temperature.
     */
    @Test
    fun `Handle Thermostat Controller Request`() {

        val requestJson = readJson("thermostat_controller_directive.json")

        /*
         * Return mock data if the restApi is called.
         */

        val restApiMock = mock(RestApi::class.java)

        val callMock = mock(Call::class.java) as Call<Unit>

        `when`(restApiMock.setDeviceTargetTemperature("my_heating", 25)).thenReturn(callMock)
        `when`(callMock.execute()).thenReturn(Response.success(null))

        /*
         * Check response
         */

        val actualResponseJson = handler.handleRequestJson(requestJson, restApiMock)

        val expectedResponseJson = readJson("thermostat_controller_response.json")

        assertEquals(expectedResponseJson, clearLineBreaks(actualResponseJson))

        /*
         * Verify mocks
         */

        verify(restApiMock, times(1)).setDeviceTargetTemperature("my_heating", 25)
        verifyNoMoreInteractions(restApiMock)
    }

    /*
     * Example test to verify GSON is working
     */
    @Test
    fun `Parse AlexaRequest object from Discovery request`() {

        val requestJson = readJson("discovery_directive.json")

        val alexaRequest = Gson().fromJson(requestJson, AlexaRequest::class.java)

        assertNotNull(alexaRequest, "Parsing into object failed.")

        val directive = alexaRequest.directive

        assertNotNull(directive)

        // check header
        assertNotNull(directive.header)
        assertEquals("Alexa.Discovery", directive.header.namespace)
        assertEquals("Discover", directive.header.name)
        assertEquals("3", directive.header.payloadVersion)
        assertEquals("<message id>", directive.header.messageId)

        // check endpoint - should not exist
        assertNull(directive.endpoint)

        // check payload
        assertNotNull(directive.payload)
        assertNotNull(directive.payload.scope)
        assertEquals("BearerToken", directive.payload.scope!!.type)
        assertEquals("access-token-from-skill", directive.payload.scope!!.token)
    }

    /*
     * Helper methods
     */

    /**
     * Removes Line Breaks for Windows and generic, because they are not relevant here.
     */
    private fun clearLineBreaks(string: String): String {
        return string.replace("\r", "").replace("\n", "")
    }

    private fun readJson(fileName: String): String {

        val jsonResource = AlexaHandlerTest::class.java.getResource(fileName)

        assertNotNull(jsonResource, "Resource $fileName not found.")

        val json = jsonResource.readText()

        assertNotNull(json, "readText() of $fileName failed.")

        return clearLineBreaks(json)
    }
}